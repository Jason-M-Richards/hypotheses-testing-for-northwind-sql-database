


### The Database

Northwind database--a free, open-source dataset created by Microsoft containing data from a fictional company. 

<img src='https://raw.githubusercontent.com/learn-co-curriculum/dsc-2-final-project/master/Northwind_ERD.png'>
